# Continuously Deploying Django app to AWS EC2 with Docker and GitLab

Configure GitLab CI to continuously deploy a Django and Docker application to AWS EC2.

### Development

Run locally:

```sh
$ docker-compose up -d --build

$ docker-compose exec web sh

```
### Debugging locally

1. stop the web docker
2. add breakpoint (example: with pdb)
3. docker-compose run --service-ports web
4. debug


### Access the database through the ec2 instance: (first you must set the private key on your local machine)
1. ssh -o StrictHostKeyChecking=no ec2-user@<YOUR_INSTANCE_IP>
2. psql -h <YOUR_RDS_ENDPOINT> -U webapp -d django_prod
3. Enter the password.
4. \q once done.

### Set private key on local machine
1. export PRIVATE_KEY=....
2. ssh-add - <<< "${PRIVATE_KEY}"

### SSH into the instance using Key Pair:
1. ssh -i your-key-pair.pem ec2-user@<PUBLIC-IP-ADDRESS>

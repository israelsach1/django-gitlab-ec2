from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase

class RegisterationTestCase(APITestCase):
    def test_registration_and_login(self):
        data = {
            "username": "israel_test",
            "password": "I67614943i",
            "email": "israel_test@gmail.com",
            "first_name": "Israeltest",
            "last_name": "Sachartov"
        }

        response = self.client.post('/account/api/register', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = {
            "username": "israel_test",
            "password": "I67614943i"
        }
        response = self.client.post('/api/token/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('access' in response.json())
